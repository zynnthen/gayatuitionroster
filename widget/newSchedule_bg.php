<?php

require_once("../function/database.php");
session_start();

$subject = $_POST['subject'];
$level = $_POST['class'];
$class = "1/" . $_POST['month'] . "/" . $_POST['year'];
$c1 = $_POST['c1'];
$c2 = $_POST['c2'];
$c3 = $_POST['c3'];
$c4 = $_POST['c4'];

// query subject type
$query = "SELECT * FROM Subject WHERE SubjectID = '" . $subject . "' ";
$result = mysql_query($query);
$row = mysql_fetch_array($result);
$level = $row['Type'] . $level;

$query = "INSERT INTO Class ( Subject_SubjectID, Level, ClassesMY, ClassA, ClassB, ClassC, ClassD )
            VALUES ( '$subject', '$level', STR_TO_DATE('$class', '%d/%m/%Y'), STR_TO_DATE('$c1', '%d/%m/%Y'), STR_TO_DATE('$c2', '%d/%m/%Y'),"
        . " STR_TO_DATE('$c3', '%d/%m/%Y'), STR_TO_DATE('$c4', '%d/%m/%Y') )";
mysql_query($query);

// check is the subjectID belong to piano classes
$queryPiano = "SELECT * FROM Subject WHERE SubjectID = $subject AND LOWER(Description) LIKE 'piano%'";
$resultPiano = mysql_query($queryPiano);
$num_row = mysql_num_rows($resultPiano);
if ($num_row > 0) {
    $queryEnrollment = "SELECT * FROM Student, StudentSubject "
            . "Where Student.StudentID=StudentSubject.StudentID "
            . "AND StudentSubject.SubjectID=$subject ";
} else {
    // for primary and secondary class non-package query
    $queryEnrollment = "SELECT * FROM Student, StudentSubject "
            . "Where Student.StudentID=StudentSubject.StudentID "
            . "AND StudentSubject.SubjectID=$subject "
            . "AND Student.Level='$level' ";
}

$info = mysql_query($queryEnrollment);
while ($row = mysql_fetch_assoc($info)) {
    $insertEnrollmentQuery = "INSERT INTO Enrollment ( Class_Subject_SubjectID, Class_Level, Class_ClassesMY, Student_StudentID )
            VALUES ( '$subject', '$level', STR_TO_DATE('$class', '%d/%m/%Y'), '{$row['StudentID']}' )";
    mysql_query($insertEnrollmentQuery);
}

// for package student
$queryPackage = "SELECT * FROM Subject WHERE Package = 'Yes'  ";
$resultPackage = mysql_query($queryPackage);
while ($row = mysql_fetch_array($resultPackage)) {
    $packageID = $row['SubjectID'];
    $arr = $row['PackageDesc'];
}
$packageSubject = explode(',', $arr);

foreach ($packageSubject as $packageValue) {
    if ($packageValue == $subject) {
        $package_student_query = "SELECT * FROM Student, StudentSubject "
                . "Where Student.StudentID=StudentSubject.StudentID "
                . "AND StudentSubject.SubjectID=$packageID "
                . "AND Student.Level='$level' ";

        $info = mysql_query($package_student_query);

        while ($row = mysql_fetch_assoc($info)) {
            $insertEnrollmentQuery = "INSERT INTO Enrollment ( Class_Subject_SubjectID, Class_Level, Class_ClassesMY, Student_StudentID )
                VALUES ( '$subject', '$level', STR_TO_DATE('$class', '%d/%m/%Y'), '{$row['StudentID']}' )";
            mysql_query($insertEnrollmentQuery);
        }
    }
}



$_SESSION['Add.Msg'] = "Successfully inserted";
die(header("location:../app/home.php?f=schedule&loc=newSchedule"));
?>