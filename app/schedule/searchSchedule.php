<script type="text/javascript">
    function showResult(month, year) {
        var httpxml;
        try {
            // Firefox, Opera 8.0+, Safari
            httpxml = new XMLHttpRequest();
        }
        catch (e) {
            // Internet Explorer
            try {
                httpxml = new ActiveXObject("Msxml2.XMLHTTP");
            }
            catch (e) {
                try {
                    httpxml = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e) {
                    alert("Your browser does not support AJAX!");
                    return false;
                }
            }
        }
        function stateChanged() {
            if (httpxml.readyState == 4) {
                document.getElementById("result").innerHTML = httpxml.responseText;
            }
        }
        var url = "http://localhost:8080/GayaTuitionRoster/app/schedule/searchSchedule_bg.php";
        url = url + "?month=" + month + "&year=" + year;
        httpxml.onreadystatechange = stateChanged;
        httpxml.open("GET", url, true);
        httpxml.send(null);
    }
</script>

<style>
    table {
        border-collapse: collapse;
        border-spacing:0 5px;
        width: 99%;
    }

    table, td, th {
        border: 1px solid black;
        font-family:sans-serif;
        font-size:15pt;
    }

    tr#element:hover{
        background-color: aquamarine;
        cursor: pointer;
    }
</style>

<div id="bottom">
    <h2>Edit Schedule List</h2>

    <form>
        Month:
        <select id="month" name="month" onchange="showResult(this.value, document.getElementById('year').value)">
            <option value="">-- Month --</option>
            <option value="01">January</option>
            <option value="02">February</option>
            <option value="03">March</option>
            <option value="04">April</option>
            <option value="05">May</option>
            <option value="06">June</option>
            <option value="07">July</option>
            <option value="08">August</option>
            <option value="09">September</option>
            <option value="10">October</option>
            <option value="11">November</option>
            <option value="12">December</option>
        </select>
        Year:
        <input type='number' name='year' id='year' value="<?php echo date('Y'); ?>" onchange="showResult(document.getElementById('month').value, this.value)" />
    </form>

    <div id="result">
        <?php include("searchSchedule_bg.php"); ?>
    </div>

</div>
