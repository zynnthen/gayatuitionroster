<?php
if (!isset($_GET['id'])) {
    header("location:home.php?f=schedule&loc=searchSchedule");
}

$data = explode(' ', $_GET['id']);

// $data[0] = SubjectID ;
// $data[1] = Level
// $data[2] = ClassesMY
$query = "SELECT * FROM Class WHERE Subject_SubjectID = '" . $data[0] . "' AND Level = '" . $data[1] . "' AND ClassesMY = STR_TO_DATE('$data[2]', '%Y-%m-%d')  ";
$result = mysql_query($query);
$row = mysql_fetch_array($result);

function getSubject($subject) {
    $query = "SELECT * FROM Subject WHERE SubjectID = '" . $subject . "' ";
    $result = mysql_query($query);
    $row = mysql_fetch_array($result);
    echo $row['Description'];
}

function defaultMonthHidden($monthYear, $value) {
    $month = new DateTime($monthYear);
    echo $month->format('m');
}

function defaultMonthDisplay($monthYear, $value) {
    $month = new DateTime($monthYear);
    echo $month->format('F');
}

function defaultYear($monthYear) {
    $year = new DateTime($monthYear);
    echo $year->format('Y');
}

function classesDate($classDate) {
    $date = new DateTime($classDate);
    echo $date->format('d/m/Y');
}

session_start();
function msg() {
    if (isset($_SESSION['Edit.Msg'])) {
        echo "<script>";
        echo "alert('Successfully updated')";
        echo "</script>";
        unset($_SESSION['Edit.Msg']);
    }
}
?>

<script>
    $(function() {
        $(".datepicker").datepicker();
    });

</script>

<div id="bottom">
    <a href="?f=schedule&loc=searchSchedule">Back to Schedule List</a>
    <h2>Edit Schedule</h2>

    <form id="editScheduleForm" method="post" action="../widget/editSchedule_bg.php">
        <div class="fill">
            <table>
                <tr>
                    <td>Subject / Level:
                    </td>
                    <td>
                        <input type='hidden' name='subjectid' value="<?php echo $row['Subject_SubjectID']; ?>" />
                        <input type='label' name='subject' readonly value="<?php getSubject($row['Subject_SubjectID']); ?>" />
                        <input type='label' name='level' readonly value="<?php echo $row['Level']; ?>" />
                    </td>
                </tr>

                <tr>
                    <td>
                        Month / Year:
                    </td>
                    <td>
                        <input type='hidden' name='month' id='month' value="<?php defaultMonthHidden($row['ClassesMY']); ?>" />
                        <input type='label' readonly value="<?php defaultMonthDisplay($row['ClassesMY']); ?>" />
                        <input type='label' name='year' id='year'  readonly value="<?php defaultYear($row['ClassesMY']); ?>" required />
                    </td>
                </tr>
            </table>

            <table>
                <tr>
                    <td>Class 1</td>
                    <td><input type='text' class='datepicker' name="c1" required value="<?php classesDate($row['ClassA']); ?>" /></td>
                </tr>

                <tr>
                    <td>Class 2</td>
                    <td><input type='text' class='datepicker' name="c2" required value="<?php classesDate($row['ClassB']); ?>" /></td>
                </tr>

                <tr>
                    <td>Class 3</td>
                    <td><input type='text' class='datepicker' name="c3" required value="<?php classesDate($row['ClassC']); ?>" /></td>
                </tr>

                <tr>
                    <td>Class 4</td>
                    <td><input type='text' class='datepicker' name="c4" required value="<?php classesDate($row['ClassD']); ?>" /></td>
                </tr>
            </table>


            <input type="submit" value="Save" onclick="return confirm('Confirm Save?')" />
        </div>
    </form>

    <?php msg(); ?>

</div>