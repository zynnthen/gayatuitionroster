<?php

function querySubject($str) {
    $query = mysql_query("SELECT * FROM Subject Where Type='$str' AND Package IS NULL");

    while ($info = mysql_fetch_assoc($query)) {
        echo "<tr><td>";
        echo "<input type='radio' id='{$info['SubjectID']}' name='subject' value='{$info['SubjectID']}' onChange=\"getClass('{$info['Type']}');\" >";
        echo $info['Description'];
        echo "</td></ tr>";
    }
}

session_start();

function msg() {
    if (isset($_SESSION['Add.Msg'])) {
        echo "<script>";
        echo "alert('Successfully inserted')";
        echo "</script>";
        unset($_SESSION['Add.Msg']);
    }
}
?>

<script>
    function getClass(level)
    {
        // clear the classes drop down list
        var select = document.getElementById("class");
        for (var i = select.options.length - 1; i >= 0; i--)
        {
            select.remove(i);
        }

        var arr = new Array("1", "2", "3", "4", "5", "6");

        var classes = document.getElementById("class");
        if (level === "Primary") {
            for (var i = 1; i < arr.length + 1; ++i) {
                var option = document.createElement('option');
                option.text = option.value = i;
                classes.add(option, 0);
            }
        }
        else if (level === "Secondary") {
            for (var i = 1; i < arr.length; ++i) {
                var option = document.createElement('option');
                option.text = option.value = i;
                classes.add(option, 0);
            }
        }
    }

    $(function() {
        $(".datepicker").datepicker();
    });

</script>


<div id="bottom">

    <h2>New Schedule</h2>

    <form id="newClass" method="post" action="../widget/newSchedule_bg.php">
        <div class="fill">
            <table class="fill"  style="width: 70%;">
                <tr>
                    <th class="fill">Primary</th>
                    <th class="fill">Secondary</th>
                    <th class="fill">Other</th>
                </tr>

                <tr>
                    <td class="fill" valign = "top">
                        <table>
                            <?php querySubject("Primary"); ?>
                        </table>
                    </td>

                    <td class="fill" valign = "top">
                        <table>
                            <?php querySubject("Secondary"); ?>
                        </table>
                    </td>

                    <td class="fill" valign = "top">
                        <table>
                            <?php querySubject("nil"); ?>
                        </table>
                    </td>
                </tr>
            </table>

            <table>
                <tr>
                    <td>Level:
                        <select id="class" name="class">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        Month:
                        <select id="month" name="month" required>
                            <option value="">-- Month --</option>
                            <option value="01">January</option>
                            <option value="02">February</option>
                            <option value="03">March</option>
                            <option value="04">April</option>
                            <option value="05">May</option>
                            <option value="06">June</option>
                            <option value="07">July</option>
                            <option value="08">August</option>
                            <option value="09">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                    </td>
                    <td>
                        Year:
                        <input type='number' name='year' id='year' value="<?php echo date('Y'); ?>" required />
                    </td>
                </tr>

                <tr>
                    <td>Class 1</td>
                    <td><input type='text' class='datepicker' name="c1" required /></td>
                </tr>

                <tr>
                    <td>Class 2</td>
                    <td><input type='text' class='datepicker' name="c2" required /></td>
                </tr>

                <tr>
                    <td>Class 3</td>
                    <td><input type='text' class='datepicker' name="c3" required /></td>
                </tr>

                <tr>
                    <td>Class 4</td>
                    <td><input type='text' class='datepicker' name="c4" required /></td>
                </tr>
            </table>
        </div>

        <input type="submit" value="Save" onclick="return confirm('Confirm Save?')" />
    </form>

    <?php msg(); ?>

</div>