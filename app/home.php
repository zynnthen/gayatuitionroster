<!DOCTYPE html>
<?php
error_reporting(0);
require_once("../function/database.php");

session_start();
if (!isset($_SESSION['user'])) {
    header("location:../index.php");
}
?>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pusat Tuisyen Gaya - Timetable</title>
        <script src="../js/date1.js"></script>
        <script src="../js/date2.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/style.css" />
        <link rel="stylesheet" type="text/css" href="../css/date.css" />
    </head>
    <body>

        <div id="header">
            <h1>Pusat Tuisyen Gaya - Timetable</h1>
        </div>

        <div id="container">
            <?php include("navigation.php"); ?>

            <?php
            if ($_GET['f'] && $_GET['loc']) {
                $path = $_GET['f'] . "/" . $_GET['loc'] . ".php";
                include $path;
            }
            ?>

        </div>

    </body>
</html>
