<?php
if (!isset($_GET['id'])) {
    header("location:home.php?f=attn&loc=searchSchedule");
}
$data = explode(' ', $_GET['id']);

// $data[0] = SubjectID ;
// $data[1] = Level
// $data[2] = ClassesMY
$query = "SELECT * FROM Class WHERE Subject_SubjectID = '" . $data[0] . "' AND Level = '" . $data[1] . "' AND ClassesMY = STR_TO_DATE('$data[2]', '%Y-%m-%d')  ";
$result = mysql_query($query);
$row = mysql_fetch_array($result);

function getSubjectName($subject) {
    $query = "SELECT * FROM Subject WHERE SubjectID = '" . $subject . "' ";
    $result = mysql_query($query);
    $row = mysql_fetch_array($result);
    echo $row['Description'];
}

function defaultMonth($monthYear, $value) {
    $month = new DateTime($monthYear);
    echo $month->format('F');
}

function defaultYear($monthYear) {
    $year = new DateTime($monthYear);
    echo $year->format('Y');
}

function classesDate($classDate) {
    $date = new DateTime($classDate);
    echo $date->format('d/m/Y');
}

function getStudent($id, $level, $date) {
    $query = "SELECT * FROM Enrollment e, Student s "
            . "WHERE e.Class_Subject_SubjectID = '" . $id . "' "
            . "AND e.Class_Level = '" . $level . "' "
            . "AND e.Class_ClassesMY = STR_TO_DATE('$date', '%Y-%m-%d') "
            . "AND e.Student_StudentID=s.StudentID "
            . "ORDER BY s.Name  ";
    $result = mysql_query($query);
    while ($info = mysql_fetch_assoc($result)) {
        echo "<tr>";
        echo "<td class='fill'>{$info['Name']}<input type='hidden' name='studentid[]' value='{$info['StudentID']}' ></td>";

        $checked1 = checkAttn($info['StudentID'], 'ClassA');
        echo "<td class='fill' align='center'><input type='checkbox' name='c1[{$info['StudentID']}]' $checked1 ></td>";

        $checked2 = checkAttn($info['StudentID'], 'ClassB');
        echo "<td class='fill' align='center'><input type='checkbox' name='c2[{$info['StudentID']}]' $checked2 ></td>";

        $checked3 = checkAttn($info['StudentID'], 'ClassC');
        echo "<td class='fill' align='center'><input type='checkbox' name='c3[{$info['StudentID']}]' $checked3 ></td>";

        $checked4 = checkAttn($info['StudentID'], 'ClassD');
        echo "<td class='fill' align='center'><input type='checkbox' name='c4[{$info['StudentID']}]' $checked4 ></td>";

        echo "</tr>";
    }
}

function checkAttn($studentID, $class) {
    $student_attn_query = mysql_query("SELECT * FROM Enrollment Where Student_StudentID='" . $studentID . "' AND $class = 1");
    $num_row = mysql_num_rows($student_attn_query);
    if ($num_row == 1) {
        return 'checked';
    }
}

session_start();

function msg() {
    if (isset($_SESSION['Save.Msg'])) {
        echo "<script>";
        echo "alert('Successfully saved')";
        echo "</script>";
        unset($_SESSION['Save.Msg']);
    }
}
?>

<div id="bottom">
    <a href="?f=attn&loc=searchSchedule">Back to Schedule List</a>
    <h2>Attendance</h2>

    <form id="attnForm" method="post" action="../widget/setAtten_bg.php">
        <table>
            <tr>
                <td>Subject / Level:
                </td>
                <td>
                    <input type='hidden' name='subjectid' value="<?php echo $row['Subject_SubjectID']; ?>" />
                    <input type='label' name='subject' readonly value="<?php getSubjectName($row['Subject_SubjectID']); ?>" />
                    <input type='label' name='level' readonly value="<?php echo $row['Level']; ?>" />
                </td>
            </tr>

            <tr>
                <td>
                    Month / Year:
                </td>
                <td>
                    <input type='hidden' name='monthYear' id='monthYear' value="<?php echo $row['ClassesMY']; ?>" />
                    <input type='label' readonly value="<?php defaultMonth($row['ClassesMY']); ?>" />
                    <input type='label' name='year' id='year'  readonly value="<?php defaultYear($row['ClassesMY']); ?>" required />
                </td>
            </tr>
        </table>

        <table class="fill" style="width: 95%;">
            <tr>
                <th class="fill">Name</th>
                <th class="fill"><?php classesDate($row['ClassA']); ?></th>
                <th class="fill"><?php classesDate($row['ClassA']); ?></th>
                <th class="fill"><?php classesDate($row['ClassA']); ?></th>
                <th class="fill"><?php classesDate($row['ClassA']); ?></th>
            </tr>


            <?php getStudent($row['Subject_SubjectID'], $row['Level'], $row['ClassesMY']); ?>

        </table>

        <input type="submit" value="Save" onclick="return confirm('Confirm Save?')" />
    </form>

    <?php msg(); ?>
</div>