<?php
$host = 'localhost';
$username = 'root';
$password = 'root';
$db_name = "mydb";

// Connect to server and select database.
mysql_connect("$host", "$username", "$password") or die(mysql_error());
mysql_select_db("$db_name") or die(mysql_error());

$tableName = "class";
$targetpage = "http://localhost/GayaTuitionRoster/app/home.php?f=attn&loc=searchSchedule";
$limit = 15;

$num = 'num';

if (isset($_GET['month']) && isset($_GET['year'])) { // by default
    $q = "1/" . $_GET['month'] . "/" . $_GET['year'];
    $query = "SELECT COUNT(*) as $num FROM $tableName WHERE ClassesMY = STR_TO_DATE('$q', '%d/%m/%Y') ";
} else {
    $query = "SELECT COUNT(*) as $num FROM $tableName";
}
$total_result = mysql_fetch_array(mysql_query($query));
$total_result = $total_result[$num];

$stages = 3;
if (!isset($_GET['page'])) {
    $page = '1';
} else {
    $page = mysql_real_escape_string($_GET['page']);
}
if ($page) {
    $start = ($page - 1) * $limit;
} else {
    $start = 0;
}

if (isset($_GET['month']) && isset($_GET['year'])) { // by default
    $q = "1/" . $_GET['month'] . "/" . $_GET['year'];
    $query = "SELECT * FROM $tableName, Subject "
            . "WHERE Subject.SubjectID=$tableName.Subject_SubjectID "
            . "AND ClassesMY = STR_TO_DATE('$q', '%d/%m/%Y') "
            . "ORDER BY ClassesMY DESC, Type, Level, Description "
            . "LIMIT $start, $limit";
} else {
    $query = "SELECT * FROM $tableName, Subject "
            . "WHERE Subject.SubjectID=$tableName.Subject_SubjectID "
            . "ORDER BY ClassesMY DESC, Type, Level, Description "
            . "LIMIT $start, $limit";
}

// Initial page num setup
if ($page == 0) {
    $page = 1;
}
$prev = $page - 1;
$next = $page + 1;
$lastpage = ceil($total_result / $limit);
$LastPagem1 = $lastpage - 1;


$paginate = '';
if ($lastpage > 1) {

    $paginate .= "<div class='paginate'>";
    // Previous
    if ($page > 1) {
        $paginate.= "<a href='$targetpage&page=$prev'>previous</a>";
    } else {
        $paginate.= "<span class='disabled'>previous</span>";
    }

    // Pages	
    if ($lastpage < 7 + ($stages * 2)) { // Not enough pages to breaking it up
        for ($counter = 1; $counter <= $lastpage; $counter++) {
            if ($counter == $page) {
                $paginate.= "<span class='current'>$counter</span>";
            } else {
                $paginate.= "<a href='$targetpage&page=$counter'>$counter</a>";
            }
        }
    } else if ($lastpage > 5 + ($stages * 2)) { // Enough pages to hide a few?
        // Beginning only hide later pages
        if ($page < 1 + ($stages * 2)) {
            for ($counter = 1; $counter < 4 + ($stages * 2); $counter++) {
                if ($counter == $page) {
                    $paginate.= "<span class='current'>$counter</span>";
                } else {
                    $paginate.= "<a href='$targetpage&page=$counter'>$counter</a>";
                }
            }
            $paginate.= "...";
            $paginate.= "<a href='$targetpage&page=$LastPagem1'>$LastPagem1</a>";
            $paginate.= "<a href='$targetpage&page=$lastpage'>$lastpage</a>";
        }
        // Middle hide some front and some back
        else if ($lastpage - ($stages * 2) > $page && $page > ($stages * 2)) {
            $paginate.= "<a href='$targetpage&page=1'>1</a>";
            $paginate.= "<a href='$targetpage&page=2'>2</a>";
            $paginate.= "...";
            for ($counter = $page - $stages; $counter <= $page + $stages; $counter++) {
                if ($counter == $page) {
                    $paginate.= "<span class='current'>$counter</span>";
                } else {
                    $paginate.= "<a href='$targetpage&page=$counter'>$counter</a>";
                }
            }
            $paginate.= "...";
            $paginate.= "<a href='$targetpage&page=$LastPagem1'>$LastPagem1</a>";
            $paginate.= "<a href='$targetpage&page=$lastpage'>$lastpage</a>";
        }
        // End only hide early pages
        else {
            $paginate.= "<a href='$targetpage&page=1'>1</a>";
            $paginate.= "<a href='$targetpage&page=2'>2</a>";
            $paginate.= "...";
            for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++) {
                if ($counter == $page) {
                    $paginate.= "<span class='current'>$counter</span>";
                } else {
                    $paginate.= "<a href='$targetpage&page=$counter'>$counter</a>";
                }
            }
        }
    }

    // Next
    if ($page < $counter - 1) {
        $paginate.= "<a href='$targetpage&page=$next'>next</a>";
    } else {
        $paginate.= "<span class='disabled'>next</span>";
    }

    $paginate.= "</div>";
}

// pagination
echo $paginate;

function displayMonthYear($monthYear) {
    if ($monthYear != null) {
        $date = new DateTime($monthYear);
        echo $date->format('m/Y');
    }
}

function displayClass($class) {
    if ($class != null) {
        $date = new DateTime($class);
        echo $date->format('d/m/Y');
    }
}
?>

<table>
    <th>Month / Year</th>
    <th>Subject</th>
    <th>Level</th>
    <th>Class 1</th>
    <th>Class 2</th>
    <th>Class 3</th>
    <th>Class 4</th>
    <th></th>

    <?php
    
    $result = mysql_query($query);

    while ($row = mysql_fetch_array($result)) {
        ?>
        <tr id="element">
            <td> <?php displayMonthYear($row['ClassesMY']); ?> </td>
            <td> <?php echo $row['Description']; ?> </td>
            <td> <?php echo $row['Level']; ?> </td>
            <td> <?php displayClass($row['ClassA']); ?> </td>
            <td> <?php displayClass($row['ClassB']); ?> </td>
            <td> <?php displayClass($row['ClassC']); ?> </td>
            <td> <?php displayClass($row['ClassD']); ?> </td>
            <td align='center'>
                <a class='attn' href='?f=attn&loc=setAtten&id=<?php echo $row['Subject_SubjectID'] ."+". $row['Level'] ."+". $row['ClassesMY']; ?>' >
                    <img src='../images/attendance.png' name='attn' width='16' height='16'/>
                </a>
            </td>

        </tr>
    <?php } ?>
</table>
